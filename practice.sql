-- delete
DROP TABLE reviews;
-- create
CREATE TABLE reviews (
    id INT NOT NULL AUTO_INCREMENT,
    review VARCHAR(500),
    datetime_created DATETIME,
    rating INT,
    PRIMARY KEY (id)
);
-- create review
INSERT INTO reviews (review, datetime_created, rating) 
VALUES ("The songs are okay. Worth the subscription", "2023-05-03 00:00:00", 5),
       ("The songs are meh. I want BLACKPINK", "2023-01-23 00:00:00", 1),
       ("Add Bruno Mars and Lady Gaga", "2023-03-23 00:00:00", 4),
       ("I want to listen to more k-pop", "2022-09-23 00:00:00", 3),
       ("Kindly add more OPM", "2023-02-01 00:00:00", 5);

-- display all review
SELECT review FROM reviews;

-- display all review with 5 rating
SELECT review FROM reviews WHERE rating = 5;

-- display all review with 1 rating
SELECT review FROM reviews WHERE rating = 1;

-- update
UPDATE reviews SET rating = 5;