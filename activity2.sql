-- create course_db
CREATE DATABASE course_db;

-- drop course_db;
DROP DATABASE course_db;

-- re-create course_db
CREATE DATABASE course_db;

-- select course_db
USE course_db;

-- create 3 tables
CREATE TABLE students(
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(50),
    password VARCHAR(50),
    full_name VARCHAR(50),
    email VARCHAR(50),
    PRIMARY KEY (id)
);

CREATE TABLE subjects(
    id INT NOT NULL AUTO_INCREMENT,
    course_name VARCHAR(100),
    schedule VARCHAR(100),
    instructor VARCHAR(100),
    PRIMARY KEY (id)
);

CREATE TABLE enrollments(
    id INT NOT NULL AUTO_INCREMENT,
    student_id INT NOT NULL,
    subject_id INT NOT NULL,
    datetime_created DATETIME,
    PRIMARY KEY (id),
    CONSTRAINT fk_enrollments_student_id FOREIGN KEY(student_id) REFERENCES students(id),
    CONSTRAINT fk_enrollments_subject_id FOREIGN KEY(subject_id) REFERENCES subjects(id)
);

-- create a record for each
INSERT INTO students (username, password, full_name, email) 
VALUES ("Jd Suplito", "passwordA", "Jennylyn Dianna B. Suplito", "jdsuplito@gmail.com");

INSERT INTO subjects (course_name, schedule, instructor) 
VALUES ("MYSQL", "5:00PM-10:00PM", "Ms. Camille");

INSERT INTO enrollments (student_id, subject_id, datetime_created) 
VALUES (1, 1, NOW());