-- insert / create / add
INSERT INTO artists(name) VALUES ("Post malone");
INSERT INTO artists(name) VALUES ("Aegis");
INSERT INTO artists(name) VALUES ("Salbakuta");
INSERT INTO artists(name) VALUES ("Journey");
INSERT INTO artists(name) VALUES ("Taylor Swift");
INSERT INTO artists(name) VALUES ("BTS");
INSERT INTO artists(name) VALUES ("Eraserheads");

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("ESC4PE", "1981-06-31", 4);
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Fearless", "2008-11-11", 5);
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Wings", "2016-10-10", 6);
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("CUTTERPILLOW", "1995-12-08", 7);
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("HOLLYWOOD'S BLEEDING", "2019", 1);
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Halik", "1998", 2);

INSERT INTO songs(song_name, length, genre, album_id) VALUES ('Open Arms', '00:03:18', 'rock', 1);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ('You Belong With Me', '00:03:52', 'country', 2);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ('Blood Sweat & Tears', '00:03:37', 'moombahton', 3);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ('Ang Huling El Bimbo', '00:07:30', 'rock', 4);

-- read / retrieve
SELECT * FROM songs;
-- display specific field
SELECT song_name, genre FROM songs;
SELECT song_name FROM songs WHERE genre = "rock";
SELECT song_name, length FROM songs WHERE length > 300 AND genre = "moombahton";

-- update
UPDATE songs SET length = 400 WHERE song_name = "Blood Sweat & Tears";

-- delete
DELETE FROM songs WHERE genre = "moombahton" AND length > 400;
DELETE FROM songs;

-- add multiple
--INSERT INTO <table_name> (<column1>,<column2>) VALUES (value1,value2), (value1, value2);